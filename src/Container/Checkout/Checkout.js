import React from 'react';

import CheckoutSummary from '../../Components/Order/CheckoutSummary';
import {Route} from 'react-router-dom';
import ContactData from './ContactData/ContactData';
import {connect} from 'react-redux';

class Checkout extends React.Component {
  // state = {
  //   totalPrice: 0,
  // };
  // componentWillMount() {
  //   const query = new URLSearchParams(this.props.location.search);
  //   const ingredient = {};
  //   let price = 0;
  //   for (let param of query.entries()) {
  //     if (param[0] === 'price') {
  //       price = param[1];
  //     } else {
  //       ingredient[param[0]] = +[param[1]];
  //     }
  //   }
  //   this.setState({
  //     ingredient: ingredient,
  //     totalPrice: price,
  //   });
  // }
  cancleHandler = () => {
    this.props.history.goBack();
  };
  continueHandler = () => {
    this.props.history.replace('/checkout/contact-form');
  };
  render() {
    return (
      <div>
        <CheckoutSummary
          ingredient={this.props.ings}
          cancle={this.cancleHandler}
          continue={this.continueHandler}
        ></CheckoutSummary>
        <Route
          path={this.props.match.path + '/contact-form'}
          component={ContactData}
          // render={(props) => (
          //   <ContactData
          //     ingredient={this.props.ings}
          //     price={this.state.totalPrice}
          //     {...props}
          //   ></ContactData>
          // )}
        ></Route>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    ings: state.ingredient,
    price: state.totalPrice,
  };
};
export default connect(mapStateToProps)(Checkout);
