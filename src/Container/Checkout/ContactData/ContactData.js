import React from 'react';
import Button from '../../../Components/UI/Button/Button';
import style from './ContactData.module.css';
import axios from '../../../axios';
import Spinner from '../../../Components/UI/spinner/Spinner';
import Input from '../../../Components/UI/Input/Input';
import {connect} from 'react-redux';
import ErrorHandler from '../../../HOC/ErrorHandler';
import * as action from '../../../Store/actions/index';

class ContactData extends React.Component {
  state = {
    orderForm: {
      name: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'your Name',
        },
        value: '',
        validation: {
          required: true,
        },
        valid: false,
        touch: false,
      },
      email: {
        elementType: 'input',
        elementConfig: {
          type: 'email',
          placeholder: 'email',
        },
        value: '',
        validation: {
          required: true,
        },
        valid: false,
        touch: false,
      },
      street: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Street',
        },
        value: '',
        validation: {
          required: true,
        },
        valid: false,
        touch: false,
      },

      country: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'your country',
        },
        value: '',
        validation: {
          required: true,
        },
        valid: false,
        touch: false,
      },
      zipCode: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'zip code',
        },
        value: '',
        validation: {
          required: true,
          minLength: 5,
          maxLength: 5,
        },
        valid: false,
        touch: false,
      },
      deliveryMethod: {
        elementType: 'select',
        elementConfig: {
          options: [
            {value: 'fastest', displayValue: 'fastest'},
            {value: 'fas', displayValue: 'fas'},
          ],
        },

        validation: {},
        valid: true,
      },
    },
    spinner: false,
    formValid: false,
  };

  checkValidity(value, rules) {
    let isValid = true;
    if (rules.required) {
      isValid = value.trim() !== '' && isValid;
    }
    if (rules.minLength) {
      isValid = value.length >= rules.minLength && isValid;
    }
    if (rules.maxLength) {
      isValid = value.length <= rules.maxLength && isValid;
    }
    return isValid;
  }
  changeHandler = (e, id) => {
    const updatedForm = {
      ...this.state.orderForm,
    };
    const updatedElement = {...updatedForm[id]};

    updatedElement.value = e.target.value;
    updatedElement.valid = this.checkValidity(
      updatedElement.value,
      updatedElement.validation
    );
    updatedElement.touch = true;
    updatedForm[id] = updatedElement;
    let formValid = true;
    for (let id in updatedForm) {
      formValid = updatedForm[id].valid && formValid;
    }
    console.log(formValid);

    this.setState({orderForm: updatedForm, formValid: formValid});
  };

  order = (e) => {
    e.preventDefault();
    this.setState({spinner: true});
    let formData = {};
    for (let key in this.state.orderForm) {
      formData[key] = this.state.orderForm[key].value;
    }
    const orders = {
      ingredient: this.props.ings,
      price: this.props.price,
      contactData: formData,
    };
    this.props.orderBurger(orders);

    // axios
    //   .post('/orders.json', orders)
    //   .then((Response) => {
    //     this.setState({spinner: false});
    //     this.props.history.push('/');
    //   })
    //   .catch((err) => this.setState({spinner: false}));
  };

  render() {
    const formElement = [];
    for (let key in this.state.orderForm) {
      formElement.push({
        id: key,
        config: this.state.orderForm[key],
      });
    }

    let form = (
      <form onSubmit={this.order}>
        {formElement.map((el) => (
          <Input
            key={el.id}
            elementType={el.config.elementType}
            value={el.config.value}
            elementConfig={el.config.elementConfig}
            changed={(e) => this.changeHandler(e, el.id)}
            inValid={!el.config.valid}
            shouldValidate={el.config.validation}
            touch={el.config.touch}
          ></Input>
        ))}
        <Button btnType="Success" disabled={!this.state.formValid}>
          OrderNow
        </Button>
      </form>
    );
    if (this.state.spinner) {
      form = <Spinner></Spinner>;
    }
    return (
      <div className={style.contactData}>
        <h3>Contact Form</h3>

        {form}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    ings: state.ingredient,
    price: state.totalPrice,
  };
};
export const mapDispatchToProps = (dispatch) => {
  return {
    orderBurger: (orderData) => dispatch(action.orderData(orderData)),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ErrorHandler(ContactData, axios));
