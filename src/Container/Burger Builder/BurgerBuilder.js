import React, {Component} from 'react';
import axios from '../../axios';
import Wraper from '../../HOC/Wraper';
import Burger from '../../Components/Burger/Burger';
import BuildControls from '../../Components/Burger/BuildControls/BuildControls';
import Modal from '../../Components/UI/Modal/Modal';

import OrderSummary from '../../Components/Burger/BurgerIngredient/OrderSummary/OrderSummary';
import Spinner from '../../Components/UI/spinner/Spinner';
import ErrorHandler from '../../HOC/ErrorHandler';
import {connect} from 'react-redux';
import * as burgerBuilderActions from '../../Store/actions/index';

//const PRICE = {salad: 2, meat: 5, bacon: 3, cheese: 1.6};

class BurgerBuilder extends Component {
  state = {
    // ingredient: null,
    // totalPrice: 3,
    purchasable: false,
    // purchasing: false,
    // spinner: false,
    // err: false,
  };
  componentDidMount() {
    this.props.initIngredient();
    // console.log(this.props);
    // axios
    //   .get('https://myburger-4a767.firebaseio.com/ingredient.json')
    //   .then((response) => {
    //     this.setState({ingredient: response.data});
    //   })
    //   .catch((err) => {
    //     this.setState({err: true});
    //   });
  }

  purchasable = (ingredient) => {
    const sum = Object.keys(ingredient)
      .map((igkey) => {
        return ingredient[igkey];
      })
      .reduce((sum, el) => {
        return sum + el;
      }, 0);
    return sum > 0;
    //this.setState({purchasable: sum > 0});
  };

  // addIngredient = (type) => {
  //   const oldCount = this.state.ingredient[type];
  //   const updatedCount = oldCount + 1;
  //   const updatedIngredient = {
  //     ...this.state.ingredient,
  //   };
  //   updatedIngredient[type] = updatedCount;

  //   const priceAddition = PRICE[type];
  //   const newPrice = priceAddition + this.state.totalPrice;
  //   this.setState({
  //     totalPrice: newPrice,
  //     ingredient: updatedIngredient,
  //   });
  //   this.purchasable(updatedIngredient);
  // };

  // removeIngredient = (type) => {
  //   const oldCount = this.state.ingredient[type];
  //   const updatedCount = oldCount - 1;
  //   const updatedIngredient = {
  //     ...this.state.ingredient,
  //   };
  //   updatedIngredient[type] = updatedCount;
  //   const oldPrice = this.state.totalPrice;
  //   const priceDeduction = PRICE[type];
  //   const newPrice = oldPrice - priceDeduction;
  //   this.setState({
  //     totalPrice: newPrice,
  //     ingredient: updatedIngredient,
  //   });
  //   this.purchasable(updatedIngredient);
  // };

  purchaseHaldler = () => {
    this.setState({
      purchasing: true,
    });
  };
  purchaseClosed = () => {
    this.setState({
      purchasing: false,
    });
  };
  purchaseContinue = () => {
    // const queryParams = [];
    // for (let i in this.state.ingredient) {
    //   queryParams.push(
    //     encodeURIComponent(i) +
    //       '=' +
    //       encodeURIComponent(this.state.ingredient[i])
    //   );
    // }
    // queryParams.push('price=' + this.state.totalPrice);
    // const queryString = queryParams.join('&');
    // this.setState({ingredient: queryString});
    // this.props.history.push({
    //   pathname: '/checkout',
    //   search: '?' + queryString,
    // });
    this.props.history.push('/checkout');
  };

  render() {
    const disabledInfo = {
      ...this.props.ings,
    };
    for (let key in disabledInfo) {
      disabledInfo[key] = disabledInfo[key] <= 0;
    }
    let orderSummary = null;

    let burger = this.props.error ? <p>not loaded</p> : <Spinner></Spinner>;
    if (this.props.ings) {
      burger = (
        <Wraper>
          <Burger ingredient={this.props.ings}></Burger>
          <BuildControls
            addIngredient={this.props.addIngredient}
            removeIngredient={this.props.removeIngredient}
            disabled={disabledInfo}
            price={this.props.price}
            purchasable={this.purchasable(this.props.ings)}
            ordered={this.purchaseHaldler}
          ></BuildControls>
        </Wraper>
      );
      orderSummary = (
        <OrderSummary
          ingredient={this.props.ings}
          closed={this.purchaseClosed}
          continue={this.purchaseContinue}
          price={this.props.price}
        ></OrderSummary>
      );
    }
    // if (this.state.spinner) {
    //   orderSummary = <Spinner></Spinner>;
    // }

    return (
      <Wraper>
        <Modal show={this.state.purchasing} closed={this.purchaseClosed}>
          {orderSummary}
        </Modal>
        {burger}
      </Wraper>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    ings: state.ingredient,
    price: state.totalPrice,
    error: state.error,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    addIngredient: (ingName) =>
      dispatch(burgerBuilderActions.addIngredient(ingName)),
    removeIngredient: (ingName) =>
      dispatch(burgerBuilderActions.removeIngredient(ingName)),
    initIngredient: () => dispatch(burgerBuilderActions.initIngredient()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ErrorHandler(BurgerBuilder, axios));
