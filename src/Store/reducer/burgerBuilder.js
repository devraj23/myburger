import * as actionTypes from '../actions/actionTypes';
const initialState = {
  ingredient: {salad: 0, meat: 0, bacon: 0, cheese: 0},
  totalPrice: 3,
  error: false,
};
const PRICE = {salad: 2, meat: 5, bacon: 3, cheese: 1.6};
const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_INGREDIENT:
      return {
        ...state,
        ingredient: {
          ...state.ingredient,
          [action.ingName]: state.ingredient[action.ingName] + 1,
        },
        totalPrice: state.totalPrice + PRICE[action.ingName],
      };
    case actionTypes.REMOVE_INGREDIENT:
      return {
        ...state,
        ingredient: {
          ...state.ingredient,
          [action.ingName]: state.ingredient[action.ingName] - 1,
        },
        totalPrice: state.totalPrice - PRICE[action.ingName],
      };
    case actionTypes.SET_INGREDIENT:
      return {
        ...state,
        ingredient: action.ingredient,
        error: false,
      };
    case actionTypes.FETCH_FAILED:
      return {
        ...state,
        error: true,
      };
    default:
      return state;
  }
};
export default reducer;
