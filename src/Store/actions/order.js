import * as actionTypes from './actionTypes';
import axios from '../../axios';

export const purchasedSuccess = (id, orderData) => {
  return {
    type: actionTypes.PURCHASED_SUCCESS,
    orderId: id,
    orderData: orderData,
  };
};
export const purchasedFailed = (err) => {
  return {
    type: actionTypes.PURCHASED_FAILED,
    err: err,
  };
};

export const orderData = (orderData) => {
  return (dispatch) => {
    axios
      .post('/orders.json', orderData)
      .then((Response) => {
        dispatch(purchasedSuccess(Response.data, orderData));
      })
      .catch((err) => {
        dispatch(purchasedFailed(err));
      });
  };
};
