import * as actionTypes from './actionTypes';
import axios from '../../axios';

export const addIngredient = (name) => {
  return {
    type: actionTypes.ADD_INGREDIENT,
    ingName: name,
  };
};
export const removeIngredient = (name) => {
  return {
    type: actionTypes.REMOVE_INGREDIENT,
    ingName: name,
  };
};

export const setIngredient = (ingredient) => {
  return {
    type: actionTypes.SET_INGREDIENT,
    ingredient: ingredient,
  };
};
export const fetchFailed = () => {
  return {
    type: actionTypes.FETCH_FAILED,
  };
};
export const initIngredient = () => {
  return (dispatch) => {
    axios
      .get('https://myburger-4a767.firebaseio.com/ingredient.json')
      .then((response) => {
        dispatch(setIngredient(response.data));
      })
      .catch((err) => {
        dispatch(fetchFailed());
      });
  };
};
