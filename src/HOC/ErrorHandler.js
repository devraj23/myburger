import React from 'react';
import Wraper from './Wraper';
import Modal from '../Components/UI/Modal/Modal';

const ErrorHandler = (WrappedComponent, axios) => {
  return class extends React.Component {
    state = {
      error: null,
    };
    componentDidMount() {
      this.reqInterceptor = axios.interceptors.request.use((req) => {
        this.setState({error: null});
        return req;
      });
      this.resInterCeptor = axios.interceptors.response.use(
        (res) => res,
        (error) => {
          this.setState({error: error});
        }
      );
    }
    componentWillUnmount() {
      axios.interceptors.request.eject(this.reqInterceptor);
      axios.interceptors.response.eject(this.resInterceptor);
    }
    errorComfirmedHandler = () => {
      this.setState({error: null});
    };
    render() {
      return (
        <Wraper>
          <Modal show={this.state.error} closed={this.errorComfirmedHandler}>
            {this.state.error ? this.state.error.message : null}
          </Modal>
          <WrappedComponent {...this.props}></WrappedComponent>;
        </Wraper>
      );
    }
  };
};
export default ErrorHandler;
