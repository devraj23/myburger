import React from 'react';
import styles from './NavigationItem.module.css';
import {NavLink} from 'react-router-dom';
const NavigationItem = (props) => {
  return (
    <li className={styles.navigationItem}>
      <NavLink to={props.link} activeClassName={styles.active} exact>
        {props.children}
      </NavLink>
    </li>
  );
};
export default NavigationItem;
