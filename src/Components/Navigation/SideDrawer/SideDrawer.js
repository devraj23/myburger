import React from 'react';
import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import styles from './SideDrawer.module.css';
import Wraper from '../../../HOC/Wraper';
import BackDrop from '../../UI/BackDrop/BackDrop';

const SideDrawer = (props) => {
  let attachedClasses = [styles.sideDrawer, styles.close];
  if (props.open) {
    attachedClasses = [styles.sideDrawer, styles.open];
  }
  return (
    <Wraper>
      <BackDrop show={props.open} clicked={props.closed}></BackDrop>
      <div className={attachedClasses.join(' ')}>
        <div className={styles.logo}>
          <Logo></Logo>
        </div>
        <nav>
          <NavigationItems></NavigationItems>
        </nav>
      </div>
    </Wraper>
  );
};
export default SideDrawer;
