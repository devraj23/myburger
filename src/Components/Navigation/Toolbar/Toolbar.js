import React from 'react';
import styles from './Toolbar.module.css';
import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import DrawerToggle from '../SideDrawer/Drawer/DrawerToggle';
const Toolbar = (props) => {
  return (
    <header className={styles.toolbar}>
      <DrawerToggle clicked={props.toggleButton}></DrawerToggle>
      <div className={styles.logo}>
        <Logo></Logo>
      </div>

      <nav className={styles.desktopOnly}>
        <NavigationItems></NavigationItems>
      </nav>
    </header>
  );
};
export default Toolbar;
