import React from 'react';
import style from './Input.module.css';

const Input = (props) => {
  const inputClasses = [style.inputElement];
  if (props.inValid && props.shouldValidate && props.touch) {
    inputClasses.push(style.inValid);
  }

  let inputElement = null;
  switch (props.elementType) {
    case 'input':
      inputElement = (
        <input
          className={inputClasses.join(' ')}
          {...props.elementConfig}
          value={props.value}
          onChange={props.changed}
        ></input>
      );
      break;
    case 'textarea':
      inputElement = (
        <textarea
          className={inputClasses.join(' ')}
          {...props.elementConfig}
          value={props.value}
          onChange={props.changed}
        ></textarea>
      );
      break;
    case 'select':
      inputElement = (
        <select
          className={inputClasses.join(' ')}
          value={props.value}
          onChange={props.changed}
        >
          {props.elementConfig.options.map((option) => (
            <option key={option.value} value={option.value}>
              {option.displayValue}
            </option>
          ))}
        </select>
      );
      break;
    default:
      inputElement = (
        <input
          className={inputClasses.join(' ')}
          {...props.elementConfig}
          value={props.value}
          onChange={props.changed}
        ></input>
      );
  }
  return (
    <div className={style.input}>
      <label className={style.label}>{props.label}</label>
      {inputElement}
    </div>
  );
};

export default Input;
