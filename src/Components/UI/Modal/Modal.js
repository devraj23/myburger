import React from 'react';
import styles from './Modal.module.css';
import Wraper from '../../../HOC/Wraper';
import BackDrop from '../BackDrop/BackDrop';
class Modal extends React.Component {
  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextProps.show !== this.props.show ||
      nextProps.children !== this.props.children
    );
  }
  componentDidUpdate() {}

  render() {
    return (
      <Wraper>
        <BackDrop show={this.props.show} clicked={this.props.closed}></BackDrop>
        <div
          className={styles.Modal}
          style={{
            transform: this.props.show ? 'translateY(0)' : 'translate(-100vh)',
            opacity: this.props.show ? '1' : 0,
          }}
        >
          {this.props.children}
        </div>
      </Wraper>
    );
  }
}
export default Modal;
