import React from 'react';
import style from './Order.module.css';
const Order = (props) => {
  const ingredient = [];
  for (let ingredientName in props.ingredient) {
    ingredient.push({
      name: ingredientName,
      amount: props.ingredient[ingredientName],
    });
  }

  const ingredientOutput = ingredient.map((ig) => {
    return (
      <span
        key={ig.name}
        style={{
          textTransform: 'capitalize',
          display: 'inline-block',
          margin: '0 8px',
          padding: '5px',

          border: '1px solid',
        }}
      >
        {ig.name}({ig.amount})
      </span>
    );
  });
  return (
    <div className={style.order}>
      <p>ingredients:{ingredientOutput}</p>
      <p>
        Price:<strong>{props.price.toFixed(2)}</strong>
      </p>
    </div>
  );
};
export default Order;
