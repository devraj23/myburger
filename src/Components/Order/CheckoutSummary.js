import React from 'react';
import Burger from '../Burger/Burger';
import Button from '../UI/Button/Button';
import style from './CheckoutSummary.module.css';

const CheckoutSummary = (props) => {
  return (
    <div className={style.CheckoutSummary}>
      <div style={{width: '100%', margin: 'auto'}}>
        <Burger ingredient={props.ingredient}></Burger>
        <Button btnType="Danger" clicked={props.cancle}>
          CANCLE
        </Button>
        <Button btnType="Success" clicked={props.continue}>
          CONTINUE
        </Button>
      </div>
    </div>
  );
};
export default CheckoutSummary;
