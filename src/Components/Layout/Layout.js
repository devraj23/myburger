import React from 'react';
import Wraper from '../../HOC/Wraper';
import styles from './Layout.module.css';
import Toolbar from '../Navigation/Toolbar/Toolbar';
import SideDrawer from '../Navigation/SideDrawer/SideDrawer';

class Layout extends React.Component {
  state = {
    show: false,
  };
  sidedrawerClosed = () => {
    this.setState({
      show: false,
    });
  };
  toggleButton = () => {
    this.setState((prevState) => {
      return {show: !prevState.show};
    });
  };

  render() {
    return (
      <Wraper>
        <Toolbar toggleButton={this.toggleButton}></Toolbar>
        <SideDrawer
          closed={this.sidedrawerClosed}
          open={this.state.show}
        ></SideDrawer>
        <main className={styles.content}>{this.props.children}</main>
      </Wraper>
    );
  }
}
export default Layout;
