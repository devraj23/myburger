import React from 'react';
import styles from './Logo.module.css';
import burgerLogo from '../../Assets/images/burger.png';
const Logo = () => {
  return (
    <div className={styles.logo}>
      <img src={burgerLogo} alt="ddd"></img>
    </div>
  );
};
export default Logo;
