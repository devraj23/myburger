import React from 'react';
import styles from './BuildControl.module.css';
const BuildControl = (props) => (
  <div className={styles.BuildControl}>
    <div className={styles.Lable}>{props.lable}</div>
    <button
      className={styles.Less}
      onClick={props.remove}
      disabled={props.disabled}
    >
      less
    </button>
    <button className={styles.More} onClick={props.added}>
      more
    </button>
  </div>
);

export default BuildControl;
