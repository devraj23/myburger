import React from 'react';
import styles from './BuildControls.module.css';
import BuildControl from './BuildControl/BuildControl';

const controls = [
  {lable: 'Salad', type: 'salad'},
  {lable: 'Meat', type: 'meat'},
  {lable: 'Bacon', type: 'bacon'},
  {lable: 'Cheese', type: 'cheese'},
];
const BuildControls = (props) => (
  <div className={styles.buildControls}>
    <p>current price={props.price.toFixed(2)}</p>
    {controls.map((item) => (
      <BuildControl
        key={item.lable}
        lable={item.lable}
        added={() => props.addIngredient(item.type)}
        remove={() => props.removeIngredient(item.type)}
        disabled={props.disabled[item.type]}
      ></BuildControl>
    ))}
    <button
      className={styles.OrderButton}
      disabled={!props.purchasable}
      onClick={props.ordered}
    >
      ORDER
    </button>
  </div>
);

export default BuildControls;
