import React from 'react';
import Wraper from '../../../../HOC/Wraper';
import Button from '../../../UI/Button/Button';
const OrderSummary = (props) => {
  console.log(props);
  const ingredientSummary = Object.keys(props.ingredient).map((igkey) => {
    return (
      <li key={igkey}>
        <span style={{textTransform: 'capitalize'}}>{igkey}</span>:
        {props.ingredient[igkey]}
      </li>
    );
  });

  return (
    <Wraper>
      <h3>Your Order</h3>
      <p>Your Ingredients:</p>
      <ul>{ingredientSummary}</ul>
      <p>continue to checkout?</p>
      <p>
        <strong>Total Price:{props.price.toFixed(2)}</strong>
      </p>
      <Button btnType="Danger" clicked={props.closed}>
        CANCEL
      </Button>
      <Button btnType="Success" clicked={props.continue}>
        CONTINUE
      </Button>
    </Wraper>
  );
};
export default OrderSummary;
