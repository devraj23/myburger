import React from 'react';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient';
import styles from './Burger.module.css';
const Burger = (props) => {
  let transfromedIngredient = Object.keys(props.ingredient)
    .map((igKey) => {
      return [...Array(props.ingredient[igKey])].map((_, i) => {
        return (
          <BurgerIngredient key={igKey + i} type={igKey}></BurgerIngredient>
        );
      });
    })
    .reduce((arr, el) => {
      return arr.concat(el);
    }, []);
  if (transfromedIngredient.length === 0) {
    transfromedIngredient = <p>please add some items</p>;
  }

  return (
    <div className={styles.burger}>
      <BurgerIngredient type="breade-top"></BurgerIngredient>
      {transfromedIngredient}
      <BurgerIngredient type="breade-bottom"></BurgerIngredient>
    </div>
  );
};
export default Burger;
