import React from 'react';
import Layout from './Components/Layout/Layout';
import BurgerBuilder from './Container/Burger Builder/BurgerBuilder';
import Checkout from './Container/Checkout/Checkout';
import {Switch, Route} from 'react-router-dom';
import Orders from './Container/Orders/Orders';

function App() {
  return (
    <div>
      <Layout>
        <Switch>
          <Route path="/checkout" component={Checkout}></Route>
          <Route path="/orders" component={Orders}></Route>
          <Route path="/" exact component={BurgerBuilder}></Route>
        </Switch>
      </Layout>
    </div>
  );
}

export default App;
